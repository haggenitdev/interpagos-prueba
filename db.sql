-- --------------------------------------------------------
-- Host:                         dev.haggen-it.com
-- Versión del servidor:         10.2.31-MariaDB - MariaDB Server
-- SO del servidor:              Linux
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para devhaggenit_interpagos_prueba
CREATE DATABASE IF NOT EXISTS `devhaggenit_interpagos_prueba` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `devhaggenit_interpagos_prueba`;

-- Volcando estructura para tabla devhaggenit_interpagos_prueba.clients
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_number` varchar(20) NOT NULL COMMENT 'Número de cédula.',
  `last_name_1` varchar(20) NOT NULL,
  `last_name_2` varchar(20) DEFAULT NULL,
  `first_name_1` varchar(20) NOT NULL,
  `first_name_2` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cedula_UNIQUE` (`id_number`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para procedimiento devhaggenit_interpagos_prueba.clients_create
DELIMITER //
CREATE PROCEDURE `clients_create`(IN idnumber VARCHAR(20), lastname1 VARCHAR(20), lastname2 VARCHAR(20), firstname1 VARCHAR(20), firstname2 VARCHAR(20))
BEGIN
    INSERT INTO clients (id_number, last_name_1, last_name_2, first_name_1, first_name_2)
    VALUES (idnumber, lastname1, lastname2, firstname1, firstname2);
END//
DELIMITER ;

-- Volcando estructura para procedimiento devhaggenit_interpagos_prueba.clients_delete
DELIMITER //
CREATE PROCEDURE `clients_delete`(IN clientid INT)
BEGIN
    DELETE FROM clients
    WHERE id=clientid;
END//
DELIMITER ;

-- Volcando estructura para procedimiento devhaggenit_interpagos_prueba.clients_get
DELIMITER //
CREATE PROCEDURE `clients_get`(IN clientid INT)
BEGIN
    SELECT *
    FROM clients
    WHERE id=clientid;
END//
DELIMITER ;

-- Volcando estructura para procedimiento devhaggenit_interpagos_prueba.clients_list
DELIMITER //
CREATE PROCEDURE `clients_list`()
BEGIN
    SELECT *
    FROM clients
    ORDER BY last_name_1, last_name_2, first_name_1, first_name_2;
END//
DELIMITER ;

-- Volcando estructura para procedimiento devhaggenit_interpagos_prueba.clients_update
DELIMITER //
CREATE PROCEDURE `clients_update`(IN clientid INT, IN idnumber VARCHAR(20), lastname1 VARCHAR(20), lastname2 VARCHAR(20), firstname1 VARCHAR(20), firstname2 VARCHAR(20))
BEGIN
    UPDATE clients
    SET id_number=idnumber, last_name_1=lastname1, last_name_2=lastname2, first_name_1=firstname1, first_name_2=firstname2
    WHERE id=clientid;
END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
