<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $clients = DB::select('CALL clients_list()');
        } catch(\Exception $e) {
            $response = ['error' => true, 'message' => $e->getMessage()];
            return response()->json($response, 500);
        }
        $response = ['error' => false, 'clients' => $clients];
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $rules = [
            'id_number' => 'required|min:5|max:20',
            'last_name_1' => 'required|min:2|max:20',
            'last_name_2' => 'min:2|max:20',
            'first_name_1' => 'required|min:2|max:20',
            'first_name_2' => 'min:2|max:20'
        ];
        $validation = Validator::make($input, $rules);
        if($validation->fails()) {
            $response = ['error' => true, 'message' => $validation->errors()];
            return response()->json($response, 400);
        }
        try {
            $statement = DB::connection()->getPdo()->prepare('CALL clients_create(?, ?, ?, ?, ?)');
            $statement->bindParam(1, $input['id_number']);
            $statement->bindParam(2, $input['last_name_1']);
            $statement->bindParam(3, $input['last_name_2']);
            $statement->bindParam(4, $input['first_name_1']);
            $statement->bindParam(5, $input['first_name_2']);
            $statement->execute();
        } catch(\Exception $e) {
            $response = ['error' => true, 'message' => $e->getMessage()];
            return response()->json($response, 500);
        }
        $response = ['error' => false];
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $client = DB::select('CALL clients_get(?)', [$id]);
        } catch(Exception $e) {
            $response = ['error' => true, 'message' => $e->getMessage()];
            return response()->json($response, 500);
        }
        $response = ['error' => false, 'client' => $client];
        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $rules = [
            'id_number' => 'required|min:5|max:20',
            'last_name_1' => 'required|min:2|max:20',
            'last_name_2' => 'min:2|max:20',
            'first_name_1' => 'required|min:2|max:20',
            'first_name_2' => 'min:2|max:20'
        ];
        $validation = Validator::make($input, $rules);
        if($validation->fails()) {
            $response = ['error' => true, 'message' => $validation->errors()];
            return response()->json($response, 400);
        }
        try {
            $statement = DB::connection()->getPdo()->prepare('CALL clients_update(?, ?, ?, ?, ?, ?)');
            $statement->bindParam(1, $id);
            $statement->bindParam(2, $input['id_number']);
            $statement->bindParam(3, $input['last_name_1']);
            $statement->bindParam(4, $input['last_name_2']);
            $statement->bindParam(5, $input['first_name_1']);
            $statement->bindParam(6, $input['first_name_2']);
            $statement->execute();
        } catch(\Exception $e) {
            $response = ['error' => true, 'message' => $e->getMessage()];
            return response()->json($response, 500);
        }
        $response = ['error' => false];
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $statement = DB::connection()->getPdo()->prepare('CALL clients_delete(?)');
            $statement->bindParam(1, $id);
            $statement->execute();
        } catch(\Exception $e) {
            $response = ['error' => true, 'message' => $e->getMessage()];
            return response()->json($response, 500);
        }
        $response = ['error' => false];
        return response()->json($response);
    }
}
