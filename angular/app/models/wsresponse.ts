/**
 * Represents the web service response
 */
import {Customer} from './customer.model';

export class WsResponse {

  /** The error message */
  public error?: string = null;

  /** The customers list */
  public clients?: Customer[] = null;

  /** The wanted customer */
  public client: Customer = null;
}
