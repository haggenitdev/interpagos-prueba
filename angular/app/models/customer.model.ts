/**
 * Represents the customer entity
 */
export class Customer {

  /** The primary key */
  public id?: number = null;

  /** The personal document number */
  public id_number: number = null;

  /** The first last name */
  public last_name_1: string = null;

  /** The second last name */
  public last_name_2: string = null;

  /** The primary first name */
  public first_name_1: string = null;

  /** The secondary first name */
  public first_name_2: string = null;
}
