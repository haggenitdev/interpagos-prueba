import { Component, OnInit } from '@angular/core';
import {CustomerService} from '../services/customer.service';
import {Customer} from '../models/customer.model';
import {WsResponse} from '../models/wsresponse';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  /** Customer model to view form. */
  customer: Customer;

  /** Customer model list to view table. */
  listCustomers: Customer[];

  /** It maintains registration form display status. By default it will be false. */
  showNew: Boolean = false;

  /** It will be either 'Save' or 'Update' based on operation. */
  submitType: string = 'Guardar';

  /* Begin: Starting lifecyle actions */
  /**
   * Constructor method.
   *
   * @param _customerService The service object to call the web services.
   */
  constructor(private _customerService: CustomerService) { }

  /**
   * Init life cycle attached method.
   */
  ngOnInit() {
    this.callGetCustomers();
  }
  /* End: Starting lifecyle actions */


  /* Begin: Creation actions */
  /**
   * To save the customer data filled in the form
   */
  onSave() {
    if (this.submitType === 'Guardar') {
      console.log('guardando');
      console.log(this.customer);
      this._customerService.save(this.customer).subscribe((data) => {
        console.log(data);
        alert('Cliente guardado con éxito.');
        this.callGetCustomers();
      }, (error) => {
        console.log(error.error.message);
        if (typeof error.error.message === 'string') {
          alert('Al intentar guardar el cliente se produjo el siguiente error: ' + error.error.message);
        } else {
          let message = '';
          if (error.error.message.hasOwnProperty('id_number')) {
            message = message + error.error.message.id_number[0];
          }
          if (error.error.message.hasOwnProperty('first_name_1')) {
            message = message +  error.error.message.first_name_1[0];
          }
          if (error.error.message.hasOwnProperty('first_name_2')) {
            message = message +  error.error.message.first_name_2[0];
          }
          if (error.error.message.hasOwnProperty('last_name_1')) {
            message = message +  error.error.message.last_name_1[0];
          }
          if (error.error.message.hasOwnProperty('last_name_2')) {
            message = message +  error.error.message.last_name_2[0];
          }
          alert('Al intentar guardar el cliente se produjo el siguiente error: ' + message);
        }
      });
    } else {
      // Update the existing properties values based on model.
      console.log('actualizando');
      console.log(this.customer);
      this._customerService.put(this.customer).subscribe((data) => {
        console.log(data);
        alert('Cliente actualizado con éxito.');
      }, (error) => {
        console.log(error);
        alert('Se produjo un error al actualizar el cliente.');
      });
    }
    // Hide registration entry section.
    this.showNew = false;
  }
  /* End: Creation actions */


  /* Begin: Reading and data visualization actions  */
  /**
   * Sends the message to the service component, in order to
   * retrieve customers.
   */
  callGetCustomers() {
    this._customerService.get().subscribe((data: WsResponse) => {
      // When the data.error element is true
      if (data.error) {
        alert('El servicio contestó con estado de error');
      } else {
        this.listCustomers = data.clients;
      }
    }, (error) => {
      console.log(error);
      alert('Se produjo un error al listar los clientes');
    });
  }

  /**
   * To show the customer registration form
   */
  onNew() {
    this.customer = new Customer();
    this.submitType = 'Guardar';
    this.showNew = true;
  }

  /**
   * To show customer data on the form, and prepare de update action.
   *
   * @param id The selected customer primary key
   */
  onEdit(id: number) {
    this.customer = new Customer();
    // Retrieve selected registration from list and assign to model.
    this.customer = this.listCustomers.find((c) => { return c.id === id });
    this.submitType = 'Actualizar';
    this.showNew = true;
  }

  /**
   * To hide de customer registration form
   */
  onCancel() {
    this.showNew = false;
  }
  /* End: Reading and data visualization actions  */


  /* Begin: Delete actions  */
  /**
   * To delete a customer from the database
   *
   * @param id The customer primary key to look up
   */
  onDelete(id: number) {
    if (confirm('Esta seguro de eliminar el cliente')) {
      this._customerService.delete(id).subscribe((data) => {
        alert('Cliente eliminado con éxito.');
        this.callGetCustomers();
        console.log(data);
      }, (error) => {
        console.log(error);
        alert('Se presentó un problema al eliminar el cliente.');
      });
    }
  }
  /* End: Delete actions  */
}
