import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Customer } from '../models/customer.model';

@Injectable()
export class CustomerService {

  /** Web service endpoint */
  // apiEndpoint = 'http://dev.haggen-it.com/interpagos-prueba/api';
  apiEndpoint = '/interpagos-prueba/api';

  /**
   * Constructor method
   *
   * @param httpClient
   */
  constructor(private httpClient: HttpClient) { }

  /**
   * Launch the web services call, in order to save the
   * customer data.
   *
   * @param customer The object with the data to save.
   */
  save(customer: Customer) {
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.post(this.apiEndpoint + '/clients', customer, {headers: headers});
  }

  /**
   * Launch the web services call, in order to retrieve the
   * customers data.
   */
  get() {
    // return this._listCustomers;
    return this.httpClient.get(this.apiEndpoint + '/clients');
  }

  /**
   * Launch the web services call, in order to retrieve a single
   * customer data.
   *
   * @param numberId The unique id value for the wanted customer.
   */
  getById(id) {
    return this.httpClient.get(this.apiEndpoint + '/clients/' + id);
  }

  /**
   * Launch the web services call, in order to update the
   * customer data.
   *
   * @param customer The object with data to update.
   */
  put(customer) {
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.put(this.apiEndpoint + '/clients/' + customer.id, customer, {headers: headers});
  }

  /**
   * Launch the web service call, in order to delete the
   * customer.
   *
   * @param id The customer primary key
   */
  delete(id) {
    return this.httpClient.delete(this.apiEndpoint + '/clients/' + id);
  }
}
